export const HOME = '/';
export const COURSES = '/courses';
export const STUDENTS = '/students';
export const REGISTRATIONS = '/registrations';
