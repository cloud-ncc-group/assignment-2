import React, { Component } from 'react';

import { Button, Form, Loader, Modal, Table } from 'semantic-ui-react';
import axios from 'axios';
import { server } from '../../constants/api';

class CourseTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      courses: [],
      courseCode: '',
      courseName: '',
      courseInstructorName: '',
      courseCredit: '',
      courseCategory: '',
    };

  }

  editCourse = async (course) => {

    const { courseCode, courseName, courseInstructorName, courseCredit } = this.state;

    await axios({
      method: 'patch',
      url: `${server}/courses/update`,
      data: {
        key: course.uid,
        code: courseCode !== '' ? courseCode : course.code,
        name: courseName !== '' ? courseName : course.name,
        instructor_name: courseInstructorName !== '' ? courseInstructorName : course.instructor_name,
        credit: courseCredit !== '' ? courseCredit : course.credit,
      },
    });

    this.listenCourse();
  };


  deleteCourse = async (id) => {
    await axios({
      method: 'delete',
      url: `${server}/courses/delete?key=${id}`,
    });

    this.listenCourse();
  };

  addCourse = async () => {

    const { courseCode, courseName, courseInstructorName, courseCredit } = this.state;

    try {

      await axios({
        method: 'post',
        url: `${server}/courses/create`,
        data: {
          code: courseCode,
          name: courseName,
          instructor_name: courseInstructorName,
          credit: courseCredit,
        },
      });

      this.listenCourse();

    } catch (e) {
      //setError(true);
    }
  };

  handleChange = (e, { name, value }) => this.setState({ [name]: value });

  listenCourse = async () => {
    this.setState({ loading: true, courses: [] });

    try {

      const res = await axios({
        method: 'get',
        url: `${server}/courses/read`,
      });

      const data = res.data;

      const courses = Object.keys(data).map(key => ({
        ...data[key],
        uid: key,
      }));

      this.setState({
        courses,
        loading: false,
      });


    } catch (e) {
      //setError(true);
      this.setState({ loading: false });
    }
  };

  componentDidMount() {
    this.listenCourse();
  }

  render() {
    const { courses, loading } = this.state;

    return (
      <div>
        {loading ? (
          <Loader active inline/>
        ) : (
          <div>
            <Table celled compact>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Code</Table.HeaderCell>
                  <Table.HeaderCell>Name</Table.HeaderCell>
                  <Table.HeaderCell>Instructor Name</Table.HeaderCell>
                  <Table.HeaderCell>Credit</Table.HeaderCell>
                  <Table.HeaderCell>Actions</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {courses.map(c => (
                  <Table.Row key={c.uid}>
                    <Table.Cell>{c.code}</Table.Cell>
                    <Table.Cell>{c.name}</Table.Cell>
                    <Table.Cell>{c.instructor_name}</Table.Cell>
                    <Table.Cell>{c.credit}</Table.Cell>
                    <Table.Cell collapsing>
                      <Button.Group>
                        <Modal closeIcon
                               onClose={() => this.setState({
                                 courseCode: '',
                                 courseName: '',
                                 courseInstructorName: '',
                                 courseCredit: '',
                               })}
                               onOpen={async () => {
                                 await this.setState({courseName: c.name, courseCode: c.code,
                                   courseInstructorName: c.instructor_name,
                                   courseCredit: c.credit,
                                 })
                               }}
                               trigger={<Button primary>Edit</Button>}>
                          <Modal.Header>
                            Edit Course
                          </Modal.Header>
                          <Modal.Content>
                            <Form onSubmit={() => {
                              this.editCourse(c);
                              this.setState({
                                courseCode: '',
                                courseName: '',
                                courseInstructorName: '',
                                courseCredit: '',
                              });
                            }}>
                              <Form.Input
                                name="courseCode"
                                label="Code"
                                onChange={this.handleChange}
                                value={this.state.courseCode}
                              />
                              <Form.Input
                                name="courseName"
                                label="Name"
                                onChange={this.handleChange}
                                value={this.state.courseName}
                              />
                              <Form.Input
                                name="courseInstructorName"
                                label="Instructor Name"
                                onChange={this.handleChange}
                                value={this.state.courseInstructorName}
                              />
                              <Form.Input
                                name="courseCredit"
                                label="Credit"
                                onChange={this.handleChange}
                                value={this.state.courseCredit}
                              />

                              <Form.Button content="Submit"/>
                            </Form>
                          </Modal.Content>

                        </Modal>
                        <Button negative onClick={() => this.deleteCourse(c.uid)}>Delete</Button>
                      </Button.Group>
                    </Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>

            <Modal closeIcon
                   onClose={() => this.setState({
                     courseCode: '',
                     courseName: '',
                     courseInstructorName: '',
                     courseCredit: '',
                   })}
                   trigger={<Button positive>Add Course</Button>}>
              <Modal.Header>
                Add Course
              </Modal.Header>
              <Modal.Content>
                <Form onSubmit={() => {
                  this.addCourse();
                  this.setState({
                    courseCode: '',
                    courseName: '',
                    courseInstructorName: '',
                    courseCredit: '',
                    courseCategory: '',
                  });
                }}>
                  <Form.Input
                    name="courseCode"
                    label="Code"
                    onChange={this.handleChange}
                    value={this.state.courseCode}
                  />
                  <Form.Input
                    name="courseName"
                    label="Name"
                    onChange={this.handleChange}
                    value={this.state.courseName}
                  />
                  <Form.Input
                    name="courseInstructorName"
                    label="Instructor Name"
                    onChange={this.handleChange}
                    value={this.state.courseInstructorName}
                  />
                  <Form.Input
                    name="courseCredit"
                    label="Credit"
                    onChange={this.handleChange}
                    value={this.state.courseCredit}
                  />
                  <Form.Button content="Submit"/>
                </Form>
              </Modal.Content>

            </Modal>
          </div>
        )}

      </div>
    );
  }
}

export default CourseTable;
