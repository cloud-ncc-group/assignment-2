import React from 'react';
import { Grid } from 'semantic-ui-react';
import CourseTable from './CourseTable';

const Courses = () => (
  <Grid>
    <Grid.Column>
      <CourseTable/>
    </Grid.Column>
  </Grid>
);

export default Courses;
