import React from 'react';
import { Link } from 'react-router-dom';

import { Menu } from 'semantic-ui-react';

const Navigation = () => {

  let index = 0;
  switch (window.location.pathname.toString()) {
    case "/registrations":
      index = 1;
      break;
    case "/students":
      index = 2;
      break;
    case "/courses":
      index = 3;
      break;
  }

  return (
    <div className={'normalMenu'}>
      <Menu widths={4}>
        <Menu.Item as={Link} to="/registrations"><h2>CNG
          495 - Assignment 2</h2></Menu.Item>
        <Menu.Item active={index === 1}  as={Link} to="/registrations">
          <h3>Registrations</h3></Menu.Item>
        <Menu.Item active={index === 2}  as={Link} to="/students">
          <h3>Students</h3></Menu.Item>
        <Menu.Item active={index === 3}  as={Link} to="/courses"><h3>Courses</h3>
        </Menu.Item>
      </Menu>
    </div>);
};

export default Navigation;
