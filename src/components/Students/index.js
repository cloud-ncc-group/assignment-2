import React from 'react';
import { Grid } from 'semantic-ui-react';
import StudentTable from './StudentTable';

const Students = () => (
  <Grid>
    <Grid.Column>
      <StudentTable/>
    </Grid.Column>
  </Grid>
);

export default Students;
