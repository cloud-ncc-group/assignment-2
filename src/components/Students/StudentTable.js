import React, { Component } from 'react';

import { Button, Form, Loader, Modal, Table } from 'semantic-ui-react';
import axios from 'axios';
import { server } from '../../constants/api';

class StudentTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      students: [],
      studentID: '',
      studentName: '',
      studentCGPA: '',
      studentYear: '',
      studentDepartment: '',
    };

  }

  editStudent = async (student) => {

    const { studentName, studentCGPA, studentYear, studentDepartment } = this.state;

    await axios({
      method: 'patch',
      url: `${server}/students/update`,
      data: {
        key: student.uid,
        id: student.id,
        name: studentName !== '' ? studentName : student.name,
        cgpa: studentCGPA !== '' ? studentCGPA : student.cgpa,
        year: studentYear !== '' ? studentYear : student.year,
        department: studentDepartment !== '' ? studentDepartment : student.department,
      },
    });

    this.listenStudent();
  };


  deleteStudent = async (id) => {

    await axios({
      method: 'delete',
      url: `${server}/students/delete?key=${id}`,
    });

    this.listenStudent();
  };

  addStudent = async () => {
    const { studentID, studentName, studentCGPA, studentYear, studentDepartment } = this.state;

    try {

      await axios({
        method: 'post',
        url: `${server}/students/create`,
        data: {
          id: studentID,
          name: studentName,
          cgpa: studentCGPA,
          year: studentYear,
          department: studentDepartment,
        },
      });

      this.listenStudent();

    } catch (e) {
      //setError(true);
    }
  };

  handleChange = (e, { name, value }) => this.setState({ [name]: value });


  listenStudent = async () => {
    this.setState({ loading: true, students: [] });

    try {

      const res = await axios({
        method: 'get',
        url: `${server}/students/read`,
      });

      const data = res.data;

      const students = Object.keys(data).map(key => ({
        ...data[key],
        uid: key,
      }));

      this.setState({
        students,
        loading: false,
      });


    } catch (e) {
      //setError(true);
      this.setState({ loading: false });
    }

  };

  componentDidMount() {
    this.listenStudent();
  }

  render() {
    const { students, loading } = this.state;

    return (
      <div>
        {loading ? (
          <Loader active inline/>
        ) : (
          <div>
            <Table celled compact>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>ID</Table.HeaderCell>
                  <Table.HeaderCell>Name</Table.HeaderCell>
                  <Table.HeaderCell>Department</Table.HeaderCell>
                  <Table.HeaderCell>CGPA</Table.HeaderCell>
                  <Table.HeaderCell>Year</Table.HeaderCell>
                  <Table.HeaderCell>Actions</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {students.map(s => (
                  <Table.Row key={s.uid}>
                    <Table.Cell>{s.id}</Table.Cell>
                    <Table.Cell>{s.name}</Table.Cell>
                    <Table.Cell>{s.department}</Table.Cell>
                    <Table.Cell>{s.cgpa}</Table.Cell>
                    <Table.Cell>{s.year}</Table.Cell>
                    <Table.Cell collapsing>
                      <Button.Group>
                        <Modal closeIcon
                               onClose={() => this.setState({
                                 studentID: '',
                                 studentName: '',
                                 studentCGPA: '',
                                 studentYear: '',
                                 studentDepartment: '',
                               })}
                               onOpen={async () => {
                                 await this.setState({
                                   studentID: s.id,
                                   studentName: s.name,
                                   studentCGPA: s.cgpa,
                                   studentYear: s.year,
                                   studentDepartment: s.department,
                                 });
                               }}
                               trigger={<Button primary>Edit</Button>}>
                          <Modal.Header>
                            Edit Student
                          </Modal.Header>
                          <Modal.Content>
                            <Form onSubmit={() => {
                              this.editStudent(s);
                              this.setState({
                                studentID: '',
                                studentName: '',
                                studentCGPA: '',
                                studentYear: '',
                                studentDepartment: '',
                              });
                            }}>
                              <Form.Input
                                name="studentName"
                                label="Name"
                                onChange={this.handleChange}
                                value={this.state.studentName}
                              />
                              <Form.Input
                                name="studentDepartment"
                                label="Department"
                                onChange={this.handleChange}
                                value={this.state.studentDepartment}
                              />
                              <Form.Input
                                name="studentCGPA"
                                label="CGPA"
                                onChange={this.handleChange}
                                value={this.state.studentCGPA}
                              />
                              <Form.Input
                                name="studentYear"
                                label="Year"
                                onChange={this.handleChange}
                                value={this.state.studentYear}
                              />

                              <Form.Button content="Submit"/>
                            </Form>
                          </Modal.Content>

                        </Modal>
                        <Button negative onClick={() => this.deleteStudent(s.uid)}>Delete</Button>
                      </Button.Group>
                    </Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>

            <Modal closeIcon
                   onClose={() => this.setState({
                     studentID: '',
                     studentName: '',
                     studentCGPA: '',
                     studentYear: '',
                     studentDepartment: '',
                   })}

                   trigger={<Button positive>Add Student</Button>}>
              <Modal.Header>
                Add Student
              </Modal.Header>
              <Modal.Content>
                <Form onSubmit={() => {
                  this.addStudent();
                  this.setState({
                    studentID: '',
                    studentName: '',
                    studentCGPA: '',
                    studentYear: '',
                    studentDepartment: '',
                  });
                }}>
                  <Form.Input
                    name="studentID"
                    label="ID"
                    onChange={this.handleChange}
                    value={this.state.studentID}
                  />
                  <Form.Input
                    name="studentName"
                    label="Name"
                    onChange={this.handleChange}
                    value={this.state.studentName}
                  />
                  <Form.Input
                    name="studentDepartment"
                    label="Department"
                    onChange={this.handleChange}
                    value={this.state.studentDepartment}
                  />
                  <Form.Input
                    name="studentCGPA"
                    label="CGPA"
                    onChange={this.handleChange}
                    value={this.state.studentCGPA}
                  />
                  <Form.Input
                    name="studentYear"
                    label="Year"
                    onChange={this.handleChange}
                    value={this.state.studentYear}
                  />
                  <Form.Button content="Submit"/>
                </Form>
              </Modal.Content>

            </Modal>
          </div>
        )}

      </div>
    );
  }
}

export default StudentTable;
