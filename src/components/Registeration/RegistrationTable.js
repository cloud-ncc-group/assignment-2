import React, { Component } from 'react';

import { Button, Form, Loader, Modal, Table } from 'semantic-ui-react';
import axios from 'axios';
import { server } from '../../constants/api';

class RegistrationTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      registrations: [],
      students: [],
      courses: [],
      registrationStudentID: '',
      registrationCourseCode: '',
      registrationCourseCategory: '',
    };

  }

  deleteRegistration = async (id) => {
    await axios({
      method: 'delete',
      url: `${server}/registrations/delete?key=${id}`,
    });
    this.listenRegistration();
  };


  addRegistration = async () => {

    const { registrationStudentID, registrationCourseCode, registrationCourseCategory } = this.state;

    if (registrationStudentID === '' || registrationCourseCode === '' || registrationCourseCategory === '') {
      return;
    }

    try {
      const res = await axios({
        method: 'post',
        url: `${server}/registrations/create`,
        data: {
          student_key: registrationStudentID,
          course_key: registrationCourseCode,
          category: registrationCourseCategory,
        },
      });

      this.listenRegistration();
    } catch (e) {
      //setError(true);
      this.setState({ loading: false });
    }
  };

  handleChange = (e, { name, value }) => this.setState({ [name]: value });

  editRegistration = async (register) => {

    const { registrationStudentID, registrationCourseCode, registrationCourseCategory } = this.state;

    if (registrationStudentID === '' || registrationCourseCode === '' || registrationCourseCategory === '') {
      return;
    }


    try {
      const res = await axios({
        method: 'patch',
        url: `${server}/registrations/update`,
        data: {
          key: register.uid,
          student_key: registrationStudentID,
          course_key: registrationCourseCode,
          category: registrationCourseCategory,
        },
      });

      this.listenRegistration();
    } catch (e) {
      //setError(true);
      this.setState({ loading: false });
    }

    this.listenRegistration();
  };

  listenRegistration = async () => {
    this.setState({ loading: true, registrations: [] });

    try {

      const res = await axios({
        method: 'get',
        url: `${server}/registrations/read`,
      });

      const data = res.data;

      const registrations = Object.keys(data).map(key => ({
        ...data[key],
        uid: key,
      }));

      this.setState({
        registrations,
        loading: false,
      });


    } catch (e) {
      //setError(true);
      this.setState({ loading: false });
    }
  };

  getStudents = async () => {
    try {

      const res = await axios({
        method: 'get',
        url: `${server}/students/read`,
      });

      const data = res.data;

      const students = Object.keys(data).map(key => ({
        key: data[key].id,
        text: data[key].id,
        value: key,
      }));

      this.setState({
        students,
        loading: false,
      });
    } catch (e) {
      //setError(true);

    }
  };

  getCourses = async () => {
    try {

      const res = await axios({
        method: 'get',
        url: `${server}/courses/read`,
      });

      const data = res.data;

      const courses = Object.keys(data).map(key => ({
        key: data[key].code,
        text: data[key].code,
        value: key,
      }));

      this.setState({
        courses,
        loading: false,
      });
    } catch (e) {
      //setError(true);

    }
  };

  componentDidMount() {
    this.listenRegistration();
    this.getCourses();
    this.getStudents();
  }

  render() {
    const { registrations, loading } = this.state;

    return (
      <div>
        {loading ? (
          <Loader active inline/>
        ) : (
          <div>
            <Table celled compact>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Student ID</Table.HeaderCell>
                  <Table.HeaderCell>Student Name</Table.HeaderCell>
                  <Table.HeaderCell>Course Code</Table.HeaderCell>
                  <Table.HeaderCell>Course Name</Table.HeaderCell>
                  <Table.HeaderCell>Course Category</Table.HeaderCell>
                  <Table.HeaderCell>Actions</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {registrations.map(r => (
                  <Table.Row key={r.uid}>
                    <Table.Cell>{r.student_id}</Table.Cell>
                    <Table.Cell>{r.student_name}</Table.Cell>
                    <Table.Cell>{r.course_code}</Table.Cell>
                    <Table.Cell>{r.course_name}</Table.Cell>
                    <Table.Cell>{r.category}</Table.Cell>
                    <Table.Cell collapsing>
                      <Button.Group>
                        <Modal closeIcon
                               onClose={() => this.setState({
                                 registrationCourseCode: '',
                                 registrationStudentID: '',
                                 registrationCourseCategory: '',
                               })}
                               onOpen={async () => {
                                 await this.setState({
                                   registrationCourseCode: this.state.courses.filter(item => item.key === r.course_code)[0].value,
                                   registrationStudentID: this.state.students.filter(item => item.key === r.student_id)[0].value,
                                   registrationCourseCategory: r.category,
                                 });
                               }}
                               trigger={<Button primary>Edit</Button>}>
                          <Modal.Header>
                            Edit Registration
                          </Modal.Header>
                          <Modal.Content>
                            <Form onSubmit={() => {
                              this.editRegistration(r);
                              this.setState({
                                registrationCourseCode: '',
                                registrationStudentID: '',
                                registrationCourseCategory: '',
                              });
                            }}>
                              <Form.Select
                                name="registrationStudentID"
                                label="Student ID"
                                options={this.state.students}
                                onChange={this.handleChange}
                                value={this.state.registrationStudentID}
                              />
                              <Form.Select
                                name="registrationCourseCode"
                                label="Course Code"
                                options={this.state.courses}
                                onChange={this.handleChange}
                                value={this.state.registrationCourseCode}
                              />
                              <Form.Input
                                name="registrationCourseCategory"
                                label="Category"
                                onChange={this.handleChange}
                                value={this.state.registrationCourseCategory}
                              />
                              <Form.Button content="Submit"/>
                            </Form>
                          </Modal.Content>

                        </Modal>
                        <Button negative onClick={() => this.deleteRegistration(r.uid)}>Delete</Button>
                      </Button.Group>
                    </Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>

            <Modal closeIcon trigger={<Button positive>Add Registration</Button>}
                   onClose={() => this.setState({
                     registrationCourseCode: '',
                     registrationStudentID: '',
                     registrationCourseCategory: '',
                   })}>
              <Modal.Header>
                Add Registration
              </Modal.Header>
              <Modal.Content>
                <Form onSubmit={() => {
                  this.addRegistration();
                  this.setState({
                    registrationCourseCode: '',
                    registrationStudentID: '',
                    registrationCourseCategory: '',
                  });
                }}>
                  <Form.Select
                    name="registrationStudentID"
                    label="Student ID"
                    options={this.state.students}
                    onChange={this.handleChange}
                    value={this.state.registrationStudentID}
                  />
                  <Form.Select
                    name="registrationCourseCode"
                    label="Course Code"
                    options={this.state.courses}
                    onChange={this.handleChange}
                    value={this.state.registrationCourseCode}
                  />
                  <Form.Input
                    name="registrationCourseCategory"
                    label="Category"
                    onChange={this.handleChange}
                    value={this.state.registrationCourseCategory}
                  />
                  <Form.Button content="Submit"/>
                </Form>
              </Modal.Content>

            </Modal>
          </div>
        )}

      </div>
    );
  }
}

export default RegistrationTable;
