import React from 'react';
import { Grid } from 'semantic-ui-react';
import RegistrationTable from './RegistrationTable';

const Registration = () => (
  <Grid>
    <Grid.Column>
    <RegistrationTable/>
    </Grid.Column>
  </Grid>
);

export default Registration;
