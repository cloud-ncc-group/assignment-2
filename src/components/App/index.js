import React from 'react';
import { BrowserRouter as Router, Redirect, Route } from 'react-router-dom';

import * as ROUTES from '../../constants/routes';

import { Container } from 'semantic-ui-react';

import Navigation from '../Navigation';
import Registration from '../Registeration';
import Students from '../Students';
import Courses from '../Courses';

const App = () => (
  <Router>
    <Container className="stickyContainer">
      <Navigation/>
      <Route path={ROUTES.HOME} exact component={() => <Redirect to="/registrations"/>}/>
      <Route path={ROUTES.REGISTRATIONS} component={Registration}/>
      <Route path={ROUTES.STUDENTS} component={Students}/>
      <Route path={ROUTES.COURSES} component={Courses}/>
    </Container>
  </Router>
);

export default App;
